import Vue from 'vue'
import App from './App.vue'
import {store} from './store'
import MyButton from '@mylibrary/simple-button'

let x = MyButton;
Vue.config.productionTip = !x;
Vue.config.productionTip = false
Vue.use(MyButton);

new Vue({
	render: h => h(App),
	store
}).$mount('#app')
